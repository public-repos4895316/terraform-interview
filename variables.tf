variable "gitlab_token" {
  type = string
}

variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com/api/v4/"
}

variable "teams" {
  type    = map(list(string))
  default = {
    "Devops" = [],
    "Infrastructure" = [],
    "Frontend" = [],
    "Managers" = []
  }
}

variable "repos" {
  type    = map(object({
    team = string
    visibility  = string
    default_branch = string
    non_owner_teams = map(string)
  }))
  default = {
    repo1 = {
      team = "Devops"
      visibility  = "private"
      default_branch = "main"
      non_owner_teams = {
        Infrastructure = "developer"
        Frontend = "maintainer"
      }
    }
    repo2 = {
      team            = "Infrastructure"
      visibility      = "private"
      default_branch  = "main"
      non_owner_teams = {
        #        Infrastructure = "developer"
        #        Frontend = "maintainer"
      }
    }
    repo3 = {
      team = "Frontend"
      visibility  = "private"
      default_branch = "main"
      non_owner_teams = {
#        Infrastructure = "developer"
#        Frontend = "maintainer"
      }
    }
    repo4 = {
      team = "Managers"
      visibility  = "private"
      default_branch = "main"
      non_owner_teams = {
#        Infrastructure = "developer"
#        Frontend = "maintainer"
      }
    }
    repo5 = {
      team = ""
      visibility  = "private"
      default_branch = "main"
      non_owner_teams = {
        Infrastructure = "maintainer"
        Frontend = "maintainer"
        Managers = "maintainer"
        Devops = "maintainer"
      }
    }
  }
}

