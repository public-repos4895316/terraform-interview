resource "gitlab_group" "team" {
  name        = var.name
  parent_id   = var.parent_id
  path        = var.path
  description = "Group for ${var.name}"
}

data "gitlab_user" "user" {
  for_each = toset(var.members)
  username = each.value
}
resource "gitlab_group_membership" "member" {
  for_each = toset(var.members)
  group_id     = gitlab_group.team.id
  user_id      = data.gitlab_user.user[each.key].id
  access_level = "maintainer"
  expires_at   = "2025-12-31"
}