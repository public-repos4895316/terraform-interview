variable "name" {
  type = string
  description = "the name of the team"
}

variable "parent_id" {
  type = string
  description = "the id of the parent group"
}

variable "path" {
  type = string
  description = "the group path"
}

variable "members" {
  type = list(string)
  description = "the list of team members usernames"
}