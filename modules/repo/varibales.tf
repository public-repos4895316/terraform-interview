variable "team" {
  type = string
  description = "the owner team of the repo"
}

variable "name" {
  type = string
  description = "the name of the repo"
}

variable "visibility" {
  type = string
  description = "the visibility level of the repo"
}

variable "default_branch" {
  type = string
  description = "the default branch of the repo"
}

variable "non_owner_team_access" {
  type = map(string)
  description = "a map of non owner teams access level on the repo"
}