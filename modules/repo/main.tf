# Get the group id based on the owner team name
data "gitlab_group" "owner_team" {
  full_path = var.team == "" ? "public-repos4895316" : "public-repos4895316/${var.team}"
}


resource "gitlab_project" "project" {
  name = var.name
  namespace_id = data.gitlab_group.owner_team.id
  description = "repo within the ${var.team} group"
  visibility_level = var.visibility // can be private, internal, public
  default_branch = var.default_branch
  only_allow_merge_if_all_discussions_are_resolved = true
  remove_source_branch_after_merge = true   #Configure automatic purging of branches upon merge requests
}

# Get the ids of other teams needing access to the repo
data "gitlab_group" "non_owner_teams" {
  for_each = var.non_owner_team_access
  full_path = "public-repos4895316/${each.key}"
}

# define access level for non owner teams
resource "gitlab_project_share_group" "group_access" {
  for_each = var.non_owner_team_access
  project      = gitlab_project.project.id
  group_id     = data.gitlab_group.non_owner_teams[each.key].id
  group_access = each.value
}

# Predefine Bug and Feature issue labels for each repository
resource "gitlab_project_label" "bug" {
  project     = gitlab_project.project.id
  name        = "Bug"
  description = "issue for fixing bugs"
  color       = "#ffcc00"
}

resource "gitlab_project_label" "feature" {
  project     = gitlab_project.project.id
  name        = "Feature"
  description = "issue for new features"
  color       = "#ffa500"
}

# Prevent direct pushes to the master branch
resource "gitlab_branch_protection" "branch_default" {
  project = gitlab_project.project.id
  branch = var.default_branch
  allow_force_push = false
  merge_access_level = "developer"
  push_access_level = "no one"
}

#Ensure that each team repository requires at least two approvals on PRs from the respective team
resource "gitlab_project_approval_rule" "rule" {
  project            = gitlab_project.project.id
  name               = "Need 2 approvals from owner team"
  applies_to_all_protected_branches = true
  approvals_required = 2
  group_ids          = [data.gitlab_group.owner_team.id]
}