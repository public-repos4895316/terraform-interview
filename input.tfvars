repos = {
      repo1 = {
        team            = "Devops"
        visibility      = "private"
        default_branch  = "main"
        non_owner_teams = {
          Infrastructure = "developer"
          Frontend       = "maintainer"
        }
      },
    repo2 = {
        team            = "Devops"
        visibility      = "private"
        default_branch  = "main"
        non_owner_teams = {
          Infrastructure = "developer"
        }
      }
}