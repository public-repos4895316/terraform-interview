# terraform-interview

This terraform module enables gitlab repos management via code. It is based on two self hosted sub modules : team and repo.


## Special considerations
Since personal resources are used different improvements can be further added to the module if used for entreprise level:

- Protect the gitlab token either within gitlab project variables or a vault
- Restrict terraform code apply to only pipeline jobs after MR approvals
- Some features are limited on gitlab personal account such as users creation

## How to use the module

### Add teams
In order to create teams, the teams variable is used as input. Json format can be used as follow:

```json
  teams = {
    "team1_name" = ["member_username1","member_username2"],
    "team2_name" = ["member_username3"],
    "team3_name" = [],
  }
```
For  items within the teams variable, a gitlab group having the name `team_name` is created. Within the
group, members `member_username1`, `member_username2`... are invited. 

### Add repos
In order to create projects, the repos variable is used as input. It is a map of repos to create within the 
gitlab instance. Json format can be used as follow:

```json
repos = {
      repo1 = {
        team            = "the owner team name"
        visibility      = "the visibility of the repo private or public"
        default_branch  = "the default branch name"
        non_owner_teams = "map of other teams needing access to the repo with their access level"
        {
          team1_name = access_level
          team2_name = access_level
        }
}
```

For each repo The following features are implemented (within the repo module):
- Predefine Bug and Feature issue labels 
- Automatic purging of branches upon merge requests
- Direct pushes to the master branch are prevented
- Each repository requires at least two approvals on PRs from the owner team
### Example usage

For each case, an input file can be used to customize the terraform variables responsible for creating the 
teams and repos. The sequence of terraform commands needed is given below:

```
terraform init 
terraform plan --var-file input.tfvars
terraform apply  --var-file input.tfvars --auto-approve
```
For the given examples the same commands are used. Only the input file is changing each time.
- Create 4 teams : DevOps, Infrastructure, Frontend, Managers (usernames can be added as needed within each team)
```json
  teams = {
    "Devops" = [],
    "Infrastructure" = [],
    "Frontend" = [],
    "Managers" = []
  }
}
```

- Create sample repositories owned by Devops where each team has Developer access to all repositories
```json
repos = {
    repo1 = {
      team = "Devops"
      visibility  = "private"
      default_branch = "main"
      non_owner_teams = {
         Infrastructure = "developer"
         Frontend = "developer"
         Managers = "developer"          
      }
    }
    repo2 = {
      team            = "Devops"
      visibility      = "private"
      default_branch  = "main"
      non_owner_teams = {
         Infrastructure = "developer"
         Frontend = "developer"
         Managers = "developer"
      }
    }
  }
```
