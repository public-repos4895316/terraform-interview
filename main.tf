# Create teams using a custom local module
module "teams" {
  for_each = var.teams
  source = "./modules/team"
  name  = each.key
  parent_id = "85705640"
  path        = each.key
  members = each.value
}

module "repos" {
  for_each = var.repos
  source = "./modules/repo"
  name = each.key
  team = each.value.team
  visibility = each.value.visibility
  default_branch = each.value.default_branch
  non_owner_team_access = each.value.non_owner_teams
}